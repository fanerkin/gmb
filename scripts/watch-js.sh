#!/usr/bin/env bash

watchify -t babelify -t brfs src/index.js -o dist/user.js.tmp -v & \
catw scripts/header dist/user.js.tmp scripts/footer -o dist/user.js -v
