#!/usr/bin/env bash

FILES=$(find . -path ./node_modules -prune -o -name '*.js')
jscs $FILES
jshint $FILES
