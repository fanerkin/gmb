#!/usr/bin/env node

var fs = require('fs');
var stream = require('stream');
var multistream = require('multistream');
var browserify = require('browserify');
var brfs = require('brfs');
var uglifyify = require('uglifyify');
var CleanCSS = require('clean-css');

var version = require('../package.json').version;
var output;

fs.stat('dist', function (err, stats) {
	if (err) {
		return fs.mkdir('dist', distIsPresent);
	}

	if (stats.isDirectory()) {
		return distIsPresent();
	} else {
		throw new Error('dist is present but not a directory');
	}
});

function distIsPresent() {
	output = fs.createWriteStream('dist/user.js');
	createCss();
}

function createCss() {
	fs.readdir('src/css', function (err, files) {
		if (err) {
			throw err;
		}

		new CleanCSS().minify(
			files.map(function (name) {
				return 'src/css/' + name;
			}),
			function (err, min) {
				if (err) {
					throw err;
				}

				var css = fs.createWriteStream('dist/build.css');
				css.write(min.styles);
				css.end(createOutput);
			});
	});
}

function createOutput() {
	var header = fs.readFileSync('scripts/header', 'utf8');
	var h = new stream.Readable();
	h.push(header.replace('${version}', version));
	h.push(null);

	var b = browserify('./src/', { debug: false })
		.transform(brfs)
		.transform(uglifyify)
		.bundle()
		.on('error', function (err) {
			console.log('Error : ' + err.message);
		});

	var f = fs.createReadStream('scripts/footer', 'utf8');

	multistream([h, b, f])
		.pipe(output);
}
