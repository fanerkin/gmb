#!/usr/bin/env bash

if [ ! -d dist ]; then
	mkdir dist
fi
cat src/css/*.css | \
# one line comments, multiline comments, newlines and tabs, spaces
sed -e 's/\/\*.*\*\///g
	/\/\*/,/\*\//d' | \
tr -d '\n\t' | \
sed -e 's/ *; *} */}/g
	s/ *\(!imp\|[>:,{]\) */\1/g' > dist/build.css
