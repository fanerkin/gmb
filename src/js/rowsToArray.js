function rowsToArray(data) {
	var
		quote = /["']/.exec(data)[0],
		matches = data.match(/n\(.*?\)[,)]/g),
		re = RegExp( quote + '(.*?)' + quote, 'g' );

	return matches.map(function getQuoted(el) {
		var match,
			arr = [];

		match = re.exec(el);
		while (match) {
			arr.push(match[1]);
			match = re.exec(el);
		}

		return arr;
	});
}

module.exports = rowsToArray;
