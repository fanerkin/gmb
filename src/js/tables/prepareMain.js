var qq = require('../qq'),
	e = require('../elements'),
	rowsToArray = require('../rowsToArray'),
	sort = require('./sort'),
	showMain = require('./showMain'),
	resetPartners = require('./clearSelectionFromTablePartners'),
	createTheadFromFirstRow = require('./createTheadFromFirstRow');

function prepareMain() {
	var
		table = e.tableMain,
		tbody = table.querySelector('tbody'),
		thead,
		script = table.querySelector('script').innerHTML,
		data = e.dataMain,
		d;

	function onClick(event) {
		var column,
			target = event.target;

		if (target.tagName == 'TH') {
			column = qq.slice( thead.querySelectorAll('th') )
				.indexOf(target);
			sort( data, { 8: 7, 7: 9 }[column] || column );
			showMain();
			resetPartners();
		}
	}

	thead = createTheadFromFirstRow(tbody);
	qq.unwrap(thead.getElementsByTagName('a'));
	table.insertBefore(thead, tbody);

	d = script.match(/Array\(.*\)\)/);
	if (d) {
		d = d[0];

		data.which = 1;
		data.lastIndex = 0;
		data.tourid = script.match(/tourid=[0-9]*/)[0].substr(7);
		data.teamid = script.match(/teamid=[0-9]*/)[0].substr(7);
		data.array = rowsToArray(d);
		showMain();

		table.addEventListener('click', onClick);
	}
}

module.exports = prepareMain;
