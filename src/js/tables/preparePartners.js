var	qq = require('../qq'),
	e = require('../elements'),
	rowsToArray = require('../rowsToArray'),
	sort = require('./sort'),
	showMain = require('./showMain'),
	showPartners = require('./showPartners'),
	resetPartners = require('./clearSelectionFromTablePartners'),
	createTheadFromFirstRow = require('./createTheadFromFirstRow');

function preparePartners() {
	var
		table = e.tablePartners,
		tbody = table.querySelector('tbody'),
		thead,
		tfoot = document.createElement('tfoot'),
		script = table.querySelector('script').innerHTML,
		d = script.match(/Array\(.*\)\)/)[0],
		data = e.dataPartners;

	function onClick(event) {
		var
			target = event.target,
			row, column, nick;

		if (target.tagName == 'TH') {
			column = qq.slice( thead.querySelectorAll('th') )
				.indexOf(target);
			sort( data, { 3: 4 }[column] || column );
			showPartners();
		} else if (target.tagName == 'TD' &&
				target.parentNode.parentNode.tagName != 'TFOOT'
		) {
			row = target.parentNode;
			if ( row.classList.contains('is_on') ) {
				row.classList.remove('is_on');
				showMain();
			} else {
				resetPartners();
				row.classList.add('is_on');
				nick = row.querySelector('td').innerHTML;
				showMain(nick);
			}
		}
	}

	tfoot.appendChild(tbody.querySelector('tr:last-child'));
	qq.unwrap(tfoot.getElementsByTagName('b'));
	thead = createTheadFromFirstRow(tbody);
	table.insertBefore(thead, tbody);
	table.insertBefore(tfoot, tbody);

	data.which = 2;
	data.array = rowsToArray(d);
	sort(data, 0);
	showPartners();
	table.addEventListener('click', onClick);
}

module.exports = preparePartners;
