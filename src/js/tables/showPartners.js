var
	e = require('../elements'),
	createPartners = require('./createPartners');

function showPartners() {
	var data = e.dataPartners,
		table = e.tablePartners,
		tbody = table.querySelector('tbody');

	if (data.array) {
		table.replaceChild(	createPartners(data), tbody );
	}
}

module.exports = showPartners;
