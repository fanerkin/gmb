var
	e = require('../elements'),
	createMain = require('./createMain');

function showMain(nick) {
	var data = e.dataMain,
		table = e.tableMain,
		tbody = table.querySelector('tbody');

	if (data.array) {
		table.replaceChild( createMain(data, nick), tbody );
	}
}

module.exports = showMain;
