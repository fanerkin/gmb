var qq = require('../qq');

function createTheadFromFirstRow(tbody) {
	return qq.slice(
			qq.remove(tbody.firstChild).getElementsByTagName('td')
		).reduce(function (thead, el) {
			thead.appendChild(qq.create({
				tag: 'th',
				innerHTML: el.innerHTML
			}));
			return thead;
		}, document.createElement('thead'));
}

module.exports = createTheadFromFirstRow;
