var getClassName = require('./getClassName');

// DOM templating since IE9 fails updating table's innerHTML.
function createPartners(data) {
	var array = data.array;

	function insertRow(tbody, el, i) {
		var row;

		function insertCell(innerHTML) {
			row.insertCell(-1).innerHTML = innerHTML;
		}

		row = tbody.insertRow(-1);
		// no deals
		if (typeof el[1] == 'undefined') {
			row.appendChild(document.createTextNode(el[0]));
			return tbody;
		}

		row.className = getClassName(i, el[5]);
		[0, 1, 2, 4]
			.map(function getNicks(i) { return el[i]; })
			.forEach(insertCell);

		return tbody;
	}
	return array.reduce(insertRow, document.createElement('tbody'));
}

module.exports = createPartners;
