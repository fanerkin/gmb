var getClassName = require('./getClassName');

// DOM templating since IE9 fails updating table's innerHTML.
function createMain(data, nick) {
	var array = nick ?
			data.array.filter(filterNick) :
			data.array;

	function filterNick(el) {
		return el[9] == nick;
	}

	function insertRow(tbody, el, i) {
		var row;

		function insertCell(innerHTML) {
			row.insertCell(-1).innerHTML = innerHTML;
		}

		row = tbody.insertRow(-1);
		if (typeof el[1] == 'undefined') {
			row.appendChild(document.createTextNode(el[0]));
			return tbody;
		}

		row.className = getClassName(i, el[13]);
		[
			'<nobr>' + el[0] +
			' <a href="protocol?tourid=' + data.tourid +
			'&id=' + el[14] +
			'&teamid=' + data.teamid + '">ход&nbsp;игры</a>' +
			'&nbsp;<a href="protocol?tourid=' + data.tourid +
			'&dealno=' + el[0] +
			'&teamid=' + data.teamid +
			'&line=' + el[15] +
			'">протокол</a></nobr>'
		]
		.concat(
			el[1],
			'<nobr>' + el[2].replace(/\\'/g, '"') + '</nobr>',
			[3, 4, 5, 6].map(function getData(i) {
				return el[i];
			}),
			'<a href="protocol?tourid=' + data.tourid +
				'&teamid=' + el[8] + '">' +
				el[9] + '</a>',
			el[7]
		)
		.forEach(insertCell);

		return tbody;
	}

	return array.reduce(insertRow, document.createElement('tbody'));
}

module.exports = createMain;
