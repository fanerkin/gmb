function getClassName(i, isOwn) {
	var className;

	className = (i % 2) ? 'even' : 'odd';
	if (isOwn) {
		className += ' own';
	}
	return className;
}

module.exports = getClassName;
