var e = require('../elements');

function clearSelectionFromTablePartners() {
	var selectedRow;

	if (e.tablePartners) {
		selectedRow = e.tablePartners.querySelector('.is_on');
		if (selectedRow) {
			selectedRow.classList.remove('is_on');
		}
	}
}

module.exports = clearSelectionFromTablePartners;
