function sort(data, index) {
	function compareBy(index, type) {
		switch (type) {
			case 'lex':
				return function compareLex(a, b) {
					return a[index].toLowerCase()
						.localeCompare(b[index].toLowerCase());
				};
			case 'num':
				return function compareNum(a, b) {
					return (+b[index] || 0) - (+a[index] || 0);
				};
			case 'int':
				return function compareInt(a, b) {
					return parseInt(a[index], 10) - parseInt(b[index], 10);
				};
		}
	}

	function getType(which, index) {
		return which == 1 ?
			(index === 0 ?
				'int' :
				(~[4, 5, 6].indexOf(index) ?
					'num' :
					'lex'
				)
			) :
			index === 0 ?
				'lex' :
				'num';
	}

	if (data.lastIndex === index) {
		data.array = data.array.reverse();
	} else {
		data.array = data.array.sort(
			compareBy(index, getType(data.which, index) ) );
		data.lastIndex = index;
	}
}

module.exports = sort;
