/* Makes 2 tables sortable, binds them.
*/
var
	e = require('./elements'),
	prepareMain = require('./tables/prepareMain'),
	preparePartners = require('./tables/preparePartners');

function tables() {
	var
		isCommandChampionship = e.tableMain
			.querySelectorAll('tr:first-child > td').length == 4;

	if (!isCommandChampionship) {
		prepareMain();
	}

	if (e.tablePartners) {
		preparePartners();
	}
}

module.exports = tables;
