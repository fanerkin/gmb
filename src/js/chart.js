// makes chrono link show chart.
var
	e = require('./elements'),
	qq = require('./qq'),
	showChart = require('./chart/showChart');

function chart() {
	var chrono,
		chartElem = e.chartContainer,
		show = qq.show.bind(null, chartElem);

	function showChrono(event) {
		var href = event.target.href;

		event.preventDefault();
		if ( chartElem.hasChildNodes() ) {
			show();
		} else {
			showChart(href, show);
		}
	}

	chrono = e.center.querySelector('a[href^="ratingchange"]');
	if (chrono) {
		chrono.addEventListener('click', showChrono);
	}
}

module.exports = chart;
