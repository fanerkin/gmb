/*  Tweaks page markup & style to make it normal. Puts listeners.
*/
var
	modifyCss = require('./modifyCss'),
	modifyDom = require('./modifyDom'),
	putListeners = require('./putListeners');

function layout() {
	modifyCss();
	modifyDom();
	putListeners();
}

module.exports = layout;
