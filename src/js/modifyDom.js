var
	e = require('./elements'),
	qq = require('./qq');

function modifyDom() {
	var result, place, txt, ajaxPreloader;

	qq.remove( e.center.getElementsByTagName('br') );

	// Move results from bottom to top.
	result = e.tableRez.querySelector('tr:first-child > td:nth-child(2)');
	place = e.tableRez.querySelector('tr:nth-child(2) > td:nth-child(2)');
	e.firstLine.insertAdjacentHTML(
		'afterend',
		'Результат: <b>' + result.innerHTML + '</b>. ' +
			(place ?
				'Текущее место: <b>' + place.innerHTML + '</b>. ' :
				''
			)
	);
	qq.remove(e.tableRez);

	// Create button to toggle contracts.
	// Adjust too long info text.
	if ( e.isAllTablesPresent && e.contractButton ) {
		// Since Opera doesnt support user-select.
		e.contractButton.unselectable = 'on';
		e.contractButton.classList.add('btn');
		e.tableContract.classList.add('hide');

		txt = e.contractButton.parentNode.nextSibling;
		if ( txt && txt.nodeValue.match(/Статистика/) ) {
			txt.nodeValue = 'Статистика клуба Гамблер по текущей сессии';
			txt.parentNode.appendChild( qq.create({tag: 'br'}) );
			txt.parentNode.appendChild( document.createTextNode(
				'будет доступна только по ее окончании.') );
		}
	}

	e.center.insertBefore( qq.create({tag: 'hr'}), e.tableBig );

	e.protocolContainer = qq.create({
		id: 'protocol',
		className: 'hide',
		innerHTML: '<table id="protocol-table" class="gtborder" ' +
			'cellspacing="2" cellpadding="2">' +
			'<thead><tr bgcolor="#a0c0f0">' +
				'<th>№ п/п</th>' +
				'<th width="80">N</th>' +
				'<th width="80">S</th>' +
				'<th width="80">E</th>' +
				'<th width="80">W</th>' +
				'<th colspan="2">Контракт</th>' +
				'<th>Атака</th>' +
				'<th>Взято</th>' +
				'<th align="center"></th>' +
				'<th>ИМПы</th>' +
				'<th>Статус</th>' +
			'</tr></thead>' +
			'<tbody></tbody></table>' +
			'<table></table>'
	});
	e.center.insertBefore(e.protocolContainer, e.tableBig);

	e.progressContainer = qq.create({
		id: 'progress', className: 'hide' });
	e.center.insertBefore(e.progressContainer, e.tableBig);

	e.chartContainer = qq.create({
		id: 'chart', className: 'hide' });
	e.center.insertBefore(e.chartContainer, e.tableBig);

	ajaxPreloader = qq.create({
		id: 'preloader', className: 'hide' });
	qq.ajax.setPreloader(ajaxPreloader);
	e.center.insertBefore(ajaxPreloader, e.tableBig);
}

module.exports = modifyDom;
