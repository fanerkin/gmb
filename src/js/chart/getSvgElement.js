function setAttributesNS(el, ns, attributes) {
	var attr;
	for (attr in attributes) {
		if ( attributes.hasOwnProperty(attr) ) {
			el.setAttributeNS( ns, attr, attributes[attr] );
		}
	}
}

function getSvgElement(name, ns, attributes) {
	var svgNS = 'http://www.w3.org/2000/svg',
		el = document.createElementNS(svgNS, name);

	if (attributes) {
		setAttributesNS( el, ns, attributes );
	}

	return el;
}

module.exports = getSvgElement;
