var
	getStyle = require('./getStyle'),
	getSvgElement = require('./getSvgElement'),
	getBarHeights = require('./getBarHeights'),
	getTourTypes = require('./getTourTypes'),
	getColors = require('./getColors'),
	getHrefs = require('./getHrefs');

// Creates barchart in #chart.
function createSvgBox(dataset, uin) {
	var
		maxBarHeight = 200,
		barBottom = 15,
		svgWidth = 800,
		svgHeight = maxBarHeight + barBottom,
		barPadding = 1,

		ratings = dataset.ratings,
		dates = dataset.dates,
		tourids = dataset.tourids,
		barHeights = getBarHeights(ratings, maxBarHeight),
		tourTypes = getTourTypes(dates),
		colors = getColors(barHeights, tourTypes),
		hrefs = getHrefs(tourTypes, tourids, uin),

		length = ratings.length,
		barWidth = svgWidth / length,

		xlinkNS = 'http://www.w3.org/1999/xlink',
		svg, rectangular, text, anchor,
		i;

	svg = getSvgElement('svg');
	svg.setAttribute('width', svgWidth);
	svg.setAttribute('height', svgHeight);
	svg.appendChild( getStyle() );

	for ( i = 0; i < length; i++ ) {
		anchor = getSvgElement('a', xlinkNS, {
			'xlink:title': dates[i],
			'xlink:href': hrefs[i]
		});
		rectangular = getSvgElement('rect', null, {
			x: i * barWidth,
			y: maxBarHeight - barHeights[i],
			width: barWidth - barPadding,
			height: barHeights[i] + barBottom,
			fill: colors[i]
		});
		text = getSvgElement('text', null, {
			x: i * barWidth + (barWidth - barPadding) / 2,
			y: maxBarHeight - barHeights[i] + 10,
			'font-family': 'sans-serif',
			'font-size': '9px',
			fill: '#fff',
			'text-anchor': 'middle'
		});

		text.appendChild( document.createTextNode(ratings[i]) );
		anchor.appendChild(rectangular);
		anchor.appendChild(text);
		svg.appendChild(anchor);
	}

	return svg;
}

module.exports = createSvgBox;
