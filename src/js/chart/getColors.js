function getColors(barHeights, tourTypes) {
	var brightnesses;

	function getColor(brightness, tourType) {
		var mapColors = {
				1: 'rgb(0,0,' + (brightness + 30) + ')',
				2: 'rgb(0,' + brightness + ',0)'
			};

		return mapColors[tourType] || 'rgb(' + (brightness + 30) + ',0,0)';
	}

	brightnesses = barHeights.map(function (height) {
		// 100-200
		return Math.round( height / 2 + 50  );
	});

	return brightnesses.map(function (brightness, i) {
		return getColor(brightness, tourTypes[i]);
	});
}

module.exports = getColors;
