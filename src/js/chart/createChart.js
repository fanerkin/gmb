var
	e = require('../elements'),
	createSvgBox = require('./createSvgBox');

function createChart(sKey, data, cb) {
	e.chartContainer.appendChild(
		createSvgBox(
			data,
			/uin=(\d+)/.exec(sKey)[1]
		)
	);
	cb();
}

module.exports = createChart;
