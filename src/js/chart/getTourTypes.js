function getTourTypes(dates) {
	var mapTourTypes = {
			'Нед': 1,
			'Инд': 2
		};
	return dates.map(function (date) {
		return mapTourTypes[date.substr(0, 3)] || 0;
	});
}

module.exports = getTourTypes;
