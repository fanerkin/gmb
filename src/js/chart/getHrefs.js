function getHrefs(tourTypes, tourids, uin) {
	return tourTypes.map(function (type, i) {
		return '/tours/' +
			( type ? 'protocol' : 'tours' ) +
			'?tourid=' + tourids[i] +
			'&' +
			( type ? 'teamid' : 'uin' ) +
			'=' + uin;
	});
}

module.exports = getHrefs;
