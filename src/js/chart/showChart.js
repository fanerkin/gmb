var
	qq = require('../qq'),
	createChart = require('./createChart'),
	getRatings = require('./getRatings');

function showChart(sKey, cb) {
	var data;

	data = sessionStorage.getItem(sKey);
	if (data) {
		createChart(sKey, JSON.parse(data), cb);
	} else {
		qq.ajax(sKey + '&lim=40', function (responseText) {
			data = responseText.match(/\(nnn\(".+\)\)/);
			if (data) {
				data = getRatings(data[0]);
				sessionStorage.setItem( sKey, JSON.stringify(data) );
				createChart(sKey, data, cb);
			}
		});
	}
}

module.exports = showChart;
