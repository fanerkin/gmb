var getSvgElement = require('./getSvgElement');

function getStyle() {
	var style;

	style = getSvgElement('style');
	style.setAttribute('type', 'text/css');
	style.appendChild(
		document.createTextNode(
			'rect:hover, text:hover { stroke:#fff; stroke-width:1px }'
		)
	);

	return style;
}

module.exports = getStyle;
