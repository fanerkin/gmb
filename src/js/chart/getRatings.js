// Takes HTML string containing the script with n.
// Returns object containing arrays of ratings, dates, tourids.
function getRatings(data) {
	var tournaments = data.match(/n\(.*?\),/g),
		ratings = [],
		dates = [],
		tourids = [];

	tournaments.forEach(function (tournament) {
		var values;

		values = tournament.match(/".*?"/g)
			.map(function (el) { return el.slice(1, -1); });

		ratings.unshift(+values[4]);
		dates.unshift(values[1] + ', ' + values[5]);
		tourids.unshift(+values[0]);
	});

	return {
		ratings: ratings,
		dates: dates,
		tourids: tourids
	};
}

module.exports = getRatings;
