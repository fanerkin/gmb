function getBarHeights(ratings, maxBarHeight) {
	var
		maxRating = Math.max.apply(null, ratings),
		minRating = Math.min.apply(null, ratings),
		spread = maxRating - minRating;

	return ratings.map(function (rating) {
		return (rating - minRating) / spread * maxBarHeight;
	});
}

module.exports = getBarHeights;
