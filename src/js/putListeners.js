/*  Puts event listeners for chronology, progress, protocol, chart.
*/
var
	e = require('./elements'),
	qq = require('./qq');

function putListeners() {
	function toggleTableContract() {
		qq.toggle(e.tableContract);
	}
	function clickOnProgress(event) {
		if (event.target.tagName == 'DIV') {
			qq.hide(this);
		}
	}
	function clickOnProtocol(event) {
		if (event.target.tagName != 'A') {
			qq.hide(this);
		}
	}
	function clickOnChart(event) {
		var
			target = event.target,
			tagName = target.tagName.toLowerCase(),
			anchor;

		anchor = {
			a: target,
			text: target.parentNode,
			rect: target.parentNode
		}[tagName];

		if (anchor) {
			event.preventDefault();
			event.stopPropagation();
			window.open( anchor.getAttribute('xlink:href') );
		} else {
			qq.hide(this);
		}
	}
	if (e.isAllTablesPresent && e.contractButton) {
		e.contractButton.addEventListener('click', toggleTableContract);
	}
	e.progressContainer.addEventListener('click', clickOnProgress);
	e.protocolContainer.addEventListener('click', clickOnProtocol);
	e.chartContainer.addEventListener('click', clickOnChart);
}

module.exports = putListeners;
