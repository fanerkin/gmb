var fs = require('fs');

function modifyCss() {
	var style,
		css = fs.readFileSync('./dist/build.css', 'utf8');

	style = document.createElement('style');
	style.type = 'text/css';
	style.appendChild(document.createTextNode(css));
	document.head.appendChild(style);
}

module.exports = modifyCss;
