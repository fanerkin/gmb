var
	preloaderStack = 0,
	preloaderEl,
	slice = Function.prototype.call.bind(Array.prototype.slice),
	qq,
	show = modify(showOne),
	hide = modify(hideOne),
	toggle = modify(toggleOne),
	unwrap = modify(unwrapOne),
	remove = modify(removeOne);

function showOne(el) {
	el.classList.remove('hide');
	el.classList.add('show');
	return el;
}

function hideOne(el) {
	el.classList.remove('show');
	el.classList.add('hide');
	return el;
}

function toggleOne(el) {
	if (el.classList.contains('hide')) {
		showOne(el);
	} else {
		hideOne(el);
	}
	return el;
}

function unwrapOne(el) {
	el.outerHTML = el.innerHTML;
	return el;
}

function removeOne(el) {
	if (el.parentNode) {
		el.parentNode.removeChild(el);
	}
	return el;
}

function modify(cb) {
	return function (obj) {
		if (obj.length) {
			slice(obj).forEach(cb);
		} else {
			cb(obj);
		}
		return obj;
	};
}

function create(values) {
	var el, val;

	el = document.createElement(values.tag || 'div');
	for (val in values) {
		if (values.hasOwnProperty(val) && val !== 'tag') {
			el[val] = values[val];
		}
	}
	return el;
}

function extend(receiver, giver) {
	function transfer(receiver, giver) {
		var prop;
		for (prop in giver) {
			if ( giver.hasOwnProperty(prop) &&
				!receiver.hasOwnProperty(prop)
			) {
				receiver[prop] = giver[prop];
			}
		}
	}
	transfer(receiver, giver);
	return receiver;
}

function ajax(url, callback, data) {
	if (!preloaderEl) {
		throw new Error('no preloader');
	}

	var xhr = new XMLHttpRequest(),
		timeout = setTimeout(abortRequest, 10000);

	// Stacks preloaders, shows if present.
	function switchPreloader(change) {
		switch (change) {
			case 'on':
				preloaderStack++;
				show(preloaderEl);
				break;
			case 'off':
				if (preloaderStack) {
					preloaderStack--;
					if (!preloaderStack) {
						hide(preloaderEl);
					}
				}
				break;
			default:
				preloaderStack = 0;
				hide(preloaderEl);
				break;
		}
	}

	function abortRequest() {
		xhr.abort();
		switchPreloader('off');
	}

	xhr.open((data ? 'POST' : 'GET'), url, true);
	xhr.onreadystatechange = function () {
		if (this.readyState == 4) {
			clearTimeout(timeout);
			switchPreloader('off');
			if (this.status == 200) {
				callback(xhr.responseText);
			}
		}
	};
	switchPreloader('on');
	xhr.send(data);
}

function getBody(html) {
	var doc;

	doc = document.implementation.createHTMLDocument('');
	doc.documentElement.innerHTML = html;
	return doc.body;
}

function groupBy(arr, n) {
	var
		arrNew = [];

	arr = slice(arr);
	while (arr.length) {
		arrNew.push(arr.splice(0, n));
	}
	return arrNew;
}

ajax.setPreloader = function setPreloader(el) {
	preloaderEl = el;
};

qq = {
	show: show,
	hide: hide,
	toggle: toggle,
	create: create,
	remove: remove,
	unwrap: unwrap,
	extend: extend,
	ajax: ajax,
	getBody: getBody,
	groupBy: groupBy,
	slice: slice
};

module.exports = qq;
