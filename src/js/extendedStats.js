/*  Shows popup with more stats.
*/
var
	e = require('./elements'),
	qq = require('./qq');

function extendedStats() {
	var extStatsContainer, extended;

	function showExt(event) {
		// popup with more stats
		var storageKey = this.href,
			data;

		event.preventDefault();
		event.stopPropagation();
		if (extStatsContainer) {
			qq.show(extStatsContainer);
		} else {
			qq.ajax(storageKey, function (responseText) {
				data = responseText
					.match(/Изменение рейтинга за сессию.*?\//)[0]
					.slice(0, -2);
				extStatsContainer = qq.create({id: 'extstats',
						className: 'hide', innerHTML: data});
				extStatsContainer.addEventListener('click',
					qq.hide.bind(null, extStatsContainer));
				extended.parentNode.insertBefore(extStatsContainer,
						extended);
				qq.show(extStatsContainer);
			});
		}
	}
	extended = e.tableBig.querySelector('a');
	if (extended.innerHTML == 'Расширенная статистика') {
		extended.addEventListener('click', showExt);
	}
}

module.exports = extendedStats;
