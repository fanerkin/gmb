var
	e = require('./elements'),
	qq = require('./qq'),
	extractDataProgress = require('./info/extractDataProgress'),
	renderProgress = require('./info/renderProgress'),
	renderProtocol = require('./info/renderProtocol');

function gameProgressProtocol() {
	function showDiv(event) {
		var target = event.target,
			href = target.href;

		function onProtocol() {
			qq.ajax(href, function (responseText) {
				renderProtocol(responseText);
				qq.show(e.protocolContainer);
				target.classList.add('clicked');
			});
		}

		function onProgress() {
			var value = sessionStorage.getItem(href);

			if (value) {
				renderAndShowProgress( JSON.parse(value) );
			} else {
				qq.ajax(href + '&kod=2&showkod=1', function (responseText) {
					var info = extractDataProgress(responseText);

					sessionStorage.setItem( href, JSON.stringify(info) );
					renderAndShowProgress(info);
				});
			}
		}

		function renderAndShowProgress(info) {
			renderProgress(info);
			qq.show(e.progressContainer);
			target.classList.add('clicked');
		}

		function isClickOnProgress(target) {
			// from either main or protocol tables
			return /^ход(\s|&nbsp;)игры$/.test(target.innerHTML);
		}

		if ( isClickOnProgress(target) ) {
			event.preventDefault();
			onProgress();
		} else if (target.innerHTML == 'протокол') {
			event.preventDefault();
			onProtocol();
		}
	}

	e.protocolContainer.addEventListener('click', showDiv);
	e.tableMain.parentNode.addEventListener('click', showDiv);
}

module.exports = gameProgressProtocol;
