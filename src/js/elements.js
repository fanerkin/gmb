var
	elements,

	center = document.querySelector('body > center'),
	tables = center.querySelectorAll('.gt'),
	isAllTablesPresent = (tables.length == 5),
	tableBig = tables[0],
	tableMain = tables[1],
	tableRez = tables[tables.length - 1],
	tablePartners = null,
	tableContract = null,

	firstLine = center.querySelector('.x-large'),
	contractButton = center.querySelector('.large:nth-child(2)');

tableBig.id = 'big';
tableMain.id = 'stbl';

if (isAllTablesPresent) {
	tablePartners = tables[2];
	tablePartners.id = 'stbl2';
	tableContract = tables[3];
}

elements = {
	isAllTablesPresent: isAllTablesPresent,
	dataMain: {},
	dataPartners: {},

	center: center,
	tableBig: tableBig,
	tableMain: tableMain,
	tableRez: tableRez,
	tablePartners: tablePartners,
	tableContract: tableContract,
	firstLine: firstLine,
	contractButton: contractButton,
	progressContainer: null,
	protocolContainer: null,
	chartContainer: null
};

module.exports = elements;
