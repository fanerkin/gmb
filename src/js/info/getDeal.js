var qq = require('../qq');

function getDeal(html) {
	var deal,
		re = /AN\((Сдача \d*)(Сдавал [WNES])(В зоне)/g;

	function replacer(el, p1, p2, p3) {
		return 'AN(\n' + p1 + '\n' + p2 + '\n' + p3;
	}

	deal = html.match(/\[DEAL:SHDC[^]*?\]/i)[0];
	deal = qq.create({ innerHTML: deal }).textContent;

	deal = deal.replace(re, replacer);

	return deal;
}

module.exports = getDeal;
