var extractDeal = require('./extractDeal');

// Takes DEAL.
// Returns deal HTML string.
function createDeal(dealData) {
	var
		sides = 'NWES'.split(''),
		suits = 'SHDC'.split(''),
		vulnerable,
		deal;

	deal = extractDeal(dealData);
	vulnerable = /В зоне (.*)/.exec(deal.AN)[1];

	function isVulnerable(side) {
		return ~vulnerable.indexOf(side) || vulnerable == 'ALL';
	}

	return sides.reduce(function (base, side) {
		var cards,
			names;

		names =
			'<div class="progress-name" style="color:' +
				( isVulnerable(side) ? '#fcb' : '#bcf' ) +
			'">' +
				deal[side + 'L'] +
			'</div>';

		cards = suits.reduce(function (base, suit) {
			base +=
				'<div class="progress-hand progress-' + suit + '">' +
					'<img alt="' + suit.toLowerCase() + '">' +
					deal[side + suit] +
				'</div>';
			return base;
		}, '');

		base +=
			'<div id="progress-' + side + '">' +
				names +
				cards +
			'</div>';
		return base;
	}, '<div id="progress-AN">' + deal.AN.replace(/\n/g, '<br>') + '</div>');
}

module.exports = createDeal;
