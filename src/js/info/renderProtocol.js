var
	e = require('../elements'),
	getBody = require('../qq').getBody,
	rowsToArray = require('../rowsToArray'),
	createProtocol = require('./createProtocol');

function renderProtocol(responseText) {
	var
		div = e.protocolContainer,
		table = div.firstChild,
		body, source, sourceText, match, data,
		rowArr, tourid, teamid, deal;

	// sets EW/NS and Импы/% in thead
	function setScoringAndSides(source, table) {
		var ths, tds;

		tds = source.querySelectorAll('tr:first-child > td');
		ths = table.querySelectorAll('thead th');

		ths[8].textContent = tds[8].textContent;
		ths[9].textContent = tds[9].textContent;
	}

	body = getBody(responseText);
	source = body.querySelector('.gtborder');
	sourceText = source.textContent;

	match = sourceText.match(/=new Array\(.*?\)\)/);
	if (match) {
		// ('1','33135544','5<IMG SRC=\'http://img.gambler.ru/h.gif\' height=11 width=13 border=0 alt=h>*','E','<IMG SRC=\'http://img.gambler.ru/s.gif\' height=11 width=13 border=0 alt=s>A','8','','&nbsp;','','288947','980583','1005357','981121','Валет','ventoLV','Ronny_d','irakan','800','-11')
		data = match[0].replace(/\\'/g, '"');

		rowArr = rowsToArray(data);
		tourid = /tourid=([0-9]*)/.exec(sourceText)[1];
		teamid = /teamid=([0-9]*)/.exec(sourceText)[1];
		deal = body.querySelector('.gtnowrap');

		setScoringAndSides(source, table);
		table.replaceChild(
			createProtocol(rowArr, tourid, teamid),
			table.querySelector('tbody') );
		div.replaceChild(deal, div.lastChild);
	}
}

module.exports = renderProtocol;
