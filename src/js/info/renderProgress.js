var e = require('../elements'),
	addSrcForImg = require('./addSrcForImg'),
	createTricks = require('./createTricks'),
	createDeal = require('./createDeal'),
	createBids = require('./createBids');

// Takes previously prepared data string.
// Returns progress data string.
function renderProgress(dt) {
	e.progressContainer.innerHTML =
		'<div id="progress-hands">' +
			addSrcForImg( createDeal(dt.deal) ) +
		'</div>' +
		'<div id="progress-bids-contract">' +
			'<table id="progress-bids"><tbody>' +
				addSrcForImg( createBids(dt.bid) ) +
			'</tbody></table>' +
			'<table id="progress-contract">' +
				addSrcForImg(dt.contract) +
			'</table> ' +
		'</div>' +
		'<div>' +
			'<table id="progress-tricks"><tbody>' +
				addSrcForImg( createTricks(dt.tricks) ) +
			'</tbody></table>' +
			'<textarea onfocus="this.select()" ' +
				'onMouseUp="return false">' +
				dt.deal +
			'</textarea>' +
			'<input type="text" onfocus="this.select()" ' +
				'onMouseUp="return false" ' +
				'value="' + dt.bid + '" />' +
		'</div>';
}

module.exports = renderProgress;
