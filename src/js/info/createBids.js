var suitToImgTag = require('./suitToImgTag'),
	groupBy = require('../qq').groupBy;

// Takes BID.
// Returns bidding table HTML string.
function createBids(bidData) {
	var columns = 4,
		bids, bidsGrouped, nicks,
		bidHead, bidBody;

	// [BID:4 I() H() S(W,N,E,S) B('nick4','nick1','nick2','nick3',1c!,pass,pass,pass) C()]
	bids = /B\((.*?)\)/.exec(bidData)[1].split(',');
	nicks = bids.splice(0, columns).map(function (el) {
		return el.slice(1, -1);
	});
	bidsGrouped = groupBy(bids, columns);

	function getHead(nicks) {
		return '<tr>' +
			nicks.reduce(function (base, nick) {
				return base + '<td>' + nick + '</td>';
			}, '') +
		'</tr>';
	}

	function getRow(bids) {
		return '<tr>' +
			bids.reduce(function (base, bid) {
				return base + '<td>' + getHTML(bid) + '</td>';
			}, '') +
		'</tr>';
	}

	function getHTML(bid) {
		if (/\dn/.test(bid)) {
			return bid.replace(/(\d)n(!?)/,
				'<span class="bid">&nbsp;$1БК$2&nbsp;</span>');
		} else if (/\d[sdhc]/.test(bid)) {
			return suitToImgTag(
				bid.replace(/(\d)([sdhc])(!?)/,
					'<span class="bid">&nbsp;$1$2$3&nbsp;</span>') );
		} else {
			return bid
				.replace('**', '<span class="rdbl">&nbsp;XX&nbsp;</span>')
				.replace('*', '<span class="dbl">&nbsp;X&nbsp;</span>')
				.replace('pass', '<span class="pass">&nbsp;пас&nbsp;</span>')
				.replace(/!$/, '<span class="bid">&nbsp;!&nbsp;</span>');
		}
	}

	bidHead = getHead(nicks);

	bidBody = bidsGrouped.reduce(function (base, group) {
		return base + getRow(group);
	}, '');

	return bidHead + bidBody;
}

module.exports = createBids;
