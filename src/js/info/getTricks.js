var qq = require('../qq');

function getTricks(table) {
	var
		cells = table.getElementsByTagName('td'),
		openingLead = cells[1].firstChild.innerHTML.charAt(0),
		tricks;

	tricks = qq.slice(cells, 11, -1)
		.filter(function (el, i) {return i % 5;})
		.reduce(function (base, el) {
			var image = el.querySelector('img');

			base.push(
				(el.className /*lead*/ ? 'L' : '') +
				(image ? image.getAttribute('alt') : '') +
				el.textContent
			);

			return base;
		}, []).join(',');
	return {
		lead: openingLead,
		tricks: tricks
	};
}

module.exports = getTricks;
