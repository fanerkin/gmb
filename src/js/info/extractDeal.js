// Takes DEAL.
// Returns deal object.
function extractDeal(dealData) {
	var deal = {},
		sides = 'NWES'.split(''),
		suits = 'LSHDC'.split('');

	deal.AN = /AN\(([^]*?)\)/.exec(dealData)[1];

	sides.forEach(function (side) {
		suits.forEach(function (suit) {
			deal[side + suit] =
				RegExp(side + suit + '\\((.*?)\\)').exec(dealData)[1];
		});
	});
	return deal;
}

module.exports = extractDeal;
