// Takes HTML string.
// Returns HTML string with src attribute added to all suit images.
function addSrcForImg(str) {
	function replacer(el, p1) {
		return 'alt="' + p1 + '" ' +
			'src="/images/' + p1 + '.gif"';
	}

	return str.replace(/alt="([shdc])"/g, replacer);
}

module.exports = addSrcForImg;
