var	suitToImgTag = require('./suitToImgTag'),
	groupBy = require('../qq').groupBy;

// Takes TRICK.
// Returns tricks table string.
function createTricks(trickData) {
	var
		trickRows = '',
		sides = 'WNES'.split(''),
		tricksHTML = '',
		lead, trickArr, topRow;

	function createRow(base, arr, i) {
		var cards;

		cards = arr.reduce(function (base, el) {
			var td = '<td';

			if (el.charAt(0) == 'L') {
				td += ' class="lead"';
				el = el.substr(1);
			}
			el = suitToImgTag('9' + el.charAt(0)).substr(1) +
				el.substr(1);
			td += '>' + el + '</td>';
			base += td;

			return base;
		}, '');

		base += '<tr><td>' + (++i) + '</td>' + cards + '</tr>';
		return base;
	}

	if (trickData) {
		lead = trickData.lead;
		trickArr = groupBy( trickData.tricks.split(','), 4 );
		trickRows = trickArr.reduce(createRow, '');
		// shift columns to be in the right order.
		while (sides[0] != lead) {
			sides.push( sides.shift() );
		}
		topRow = sides.reduce(function (base, el) {
			base += '<td>' + el + '</td>';
			return base;
		}, '<td></td>');
		topRow = '<tr>' + topRow + '</tr>';
		tricksHTML = topRow + trickRows;
	}
	return tricksHTML;
}

module.exports = createTricks;
