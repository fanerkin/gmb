// Takes HTML string.
// Returns string with sdhc letters replaced by img[alt].
function suitToImgTag(bidTr) {
	function replacer(el, p1, p2) {
		return p1 +	'<img alt="' + p2 + '">';
	}

	return bidTr.replace(/(\d)([shdc])/g, replacer);
}

module.exports = suitToImgTag;
