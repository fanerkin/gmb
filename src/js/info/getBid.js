var qq = require('../qq');

function getBid(html) {
	var bid;

	bid = html.match(/\[BID:[^]*?\]/i)[0];
	bid = qq.create({ innerHTML: bid }).textContent;

	return bid;
}

module.exports = getBid;
