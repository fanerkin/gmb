var
	getDeal = require('./getDeal'),
	getBid = require('./getBid'),
	getTricks = require('./getTricks'),
	getBody = require('../qq').getBody;

/*
* Takes HTML string.
* Returns object containing
* DEAL (string), BID (string), contract table (HTML) and tricks (object).
*
* DEAL example:
-------------
[DEAL:SHDC AN(
Сдача 216
Сдавал W
В зоне -) NL(nick1) NS(A K Q 8 3) NH(K 5) ND(K 3) NC(J 9 5 3) EL(nick2) ES(9 2) EH(9 8 3 2) ED(9 6 5 4 2) EC(7 4) SL(nick3) SS(J 6 4) SH(A J 10 6 4) SD(10) SC(10 8 6 2) WL(nick4) WS(10 7 5) WH(Q 7) WD(A Q J 8 7) WC(A K Q)]
--------
* BID example:
--------
[BID:4 I() H() S(W,N,E,S) B('nick4','nick1','nick2','nick3',1c!,pass,1d!,pass,1n!,2s,pass,pass,pass) C()]
----------
* tricks will be composed this way:
----------
{"lead": "W", "tricks": "Lc5,dA,....."}.
--------
* L = leading, first char = suit, second char = value.
*/
function extractDataProgress(responseText) {
	var
		body, tables,
		deal, bid, contract, tricks;

	body = getBody(responseText);
	tables = body
		.querySelector('center > table')
		.querySelectorAll('table.gt');

	deal = getDeal(body.innerHTML);
	bid = getBid(body.innerHTML);
	contract = tables[2].innerHTML;

	// If there were tricks in the deal.
	if (tables.length == 5) {
		tricks = getTricks(tables[4]);
	}
	return {
		deal: deal,
		bid: bid,
		contract: contract,
		tricks: tricks
	};
}

module.exports = extractDataProgress;
