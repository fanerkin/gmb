function createProtocol(array, tourid, teamid) {
	function insertRow(tbody, el) {
		var row;

		function insertCell(innerHTML) {
			row.insertCell(-1).innerHTML = innerHTML;
		}

		function getNicks(i) {
			return '<a href="protocol?tourid=' + tourid +
				'&teamid=' + el[i] + '">' +
				'<nobr>' + el[i + 4] + '</nobr>';
		}

		row = tbody.insertRow(-1);
		if (typeof el[1] == 'undefined') {
			row.appendChild(document.createTextNode(el[0]));
			return tbody;
		}
		if ( [9, 10, 11, 12].some(function hasOwnNick(i) {
				return el[i] == teamid;
			})
		) {
			row.className = 'even own';
		}

		[
			'<nobr>' + el[0] +
			' <a href="protocol?tourid=' + tourid +
			'&id=' + el[1] +
			'&teamid=' + teamid +
			'">ход игры</a></nobr>'
		]
		.concat(
			[9, 10, 11, 12].map(getNicks),
			[2, 3, 4, 5, 17, 18, 7]
				.map(function getData(i) { return el[i]; })
		)
		.forEach(insertCell);

		return tbody;
	}
	return array.reduce(insertRow, document.createElement('tbody'));
}

module.exports = createProtocol;
