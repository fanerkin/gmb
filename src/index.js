var
	layout = require('./js/layout'),
	chart = require('./js/chart'),
	extendedStats = require('./js/extendedStats'),
	tables = require('./js/tables'),
	gameProgressProtocol = require('./js/gameProgressProtocol');

function main() {
	layout();
	tables();
	chart();
	gameProgressProtocol();
	extendedStats();
}

main();
