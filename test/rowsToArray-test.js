var test = require('tape'),
	rowsToArray = require('../src/js/rowsToArray');

test('single quoted', function (t) {
	t.plan(1);
	t.deepEqual(
		rowsToArray("n('a', 'b'),n('c'))"),
		[['a', 'b'], ['c']]
	);
});

test('double quoted', function (t) {
	t.plan(1);
	t.deepEqual(
		rowsToArray('n("a", "b"),n("c"))'),
		[['a', 'b'], ['c']]
	);
});

test('referee decision containing parens', function (t) {
	t.plan(1);
	t.deepEqual(
		rowsToArray("n('Присуждена (<a href=\"https://qq\">Паша</a>)'),"),
		[['Присуждена (<a href=\"https://qq\">Паша</a>)']]
	);
});
