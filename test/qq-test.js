// phantomjs 1.9.x hasn't Function.prototype.bind,
// phantomjs 2 binary isn't available on linux
require('es5-shim');

var
	test = require('tape'),
	sinon = require('sinon'),
	qq = require('../src/js/qq'),
	wrapper = document.body.appendChild(qq.create({className: 'qq-test'}));

test('qq.getBody', function (t) {
	var
		body = '<div>foo</div><script>void(0);</script>';

	t.plan(1);
	t.equal(
		qq.getBody(
			'<html><head></head><body>' +
				body +
			'</body></html>').innerHTML,
		body
	);
});

test('qq.groupBy', function (t) {
	t.plan(1);
	t.deepEqual(
		qq.groupBy([1, 2, 3, 4, 5], 2),
		[[1, 2], [3, 4], [5]]
	);
});

test('qq.slice', function (t) {
	t.plan(1);
	t.deepEqual(
		qq.slice([1, 2, 3, 4, 5], 1, 3),
		[2, 3]
	);
});

test('qq.extend', function (t) {
	var
		target = {a: 1},
		source = {a: 2, b: 2};

	qq.extend(target, source);

	t.plan(2);
	t.deepEqual(target, {a: 1, b: 2});
	t.deepEqual(source, {a: 2, b: 2});
});

test('qq.create', function (t) {
	var
		elements = document.getElementsByClassName('create-test');

	wrapper.innerHTML = '';
	wrapper.appendChild(qq.create({className: 'create-test'}));
	wrapper.appendChild(qq.create({className: 'create-test'}));

	t.plan(1);
	t.equal(elements.length, 2);
});

test('qq.show', function (t) {
	var
		elements = document.getElementsByClassName('show-test');

	wrapper.innerHTML = '<div class="show-test">' +
			'<div class="show-test">';

	t.plan(2);
	qq.show(elements);
	t.equal(elements.length, 2);
	t.equal(
		qq.slice(elements).every(function (el) {
			return el.classList.contains('show') &&
				!el.classList.contains('hide');
		}),
		true
	);
});

test('qq.hide', function (t) {
	var
		elements = document.getElementsByClassName('hide-test');

	wrapper.innerHTML = '<div class="hide-test">' +
			'<div class="hide-test">';

	t.plan(2);
	qq.hide(elements);
	t.equal(elements.length, 2);
	t.equal(
		qq.slice(elements).every(function (el) {
			return el.classList.contains('hide') &&
				!el.classList.contains('show');
		}),
		true
	);
});

test('qq.toggle', function (t) {
	var
		elements = document.getElementsByClassName('toggle-test');

	wrapper.innerHTML = '<div class="toggle-test">' +
			'<div class="toggle-test">';

	t.plan(2);
	qq.toggle(elements);
	t.equal(elements.length, 2);
	t.equal(
		qq.slice(elements).every(function (el) {
			return el.classList.contains('hide') &&
				!el.classList.contains('show');
		}),
		true
	);
});

test('qq.unwrap', function (t) {
	var
		outers = document.getElementsByClassName('outer'),
		inners = document.getElementsByClassName('inner');

	wrapper.innerHTML = '<div class="outer">' +
			'<div class="inner"></div>' +
		'</div>' +
		'<div class="outer">' +
			'<div class="inner"></div>' +
		'</div>';

	t.plan(3);
	qq.unwrap(outers);
	t.equal(outers.length, 0);
	t.equal(inners.length, 2);
	t.equal(
		wrapper.innerHTML,
		'<div class="inner"></div>' +
		'<div class="inner"></div>'
	);
});

test('qq.remove', function (t) {
	var
		outers = document.getElementsByClassName('outer'),
		inners = document.getElementsByClassName('inner');

	wrapper.innerHTML = '<div class="outer">' +
			'<div class="inner"></div>' +
		'</div>' +
		'<div class="outer">' +
			'<div class="inner"></div>' +
		'</div>';

	t.plan(3);
	qq.remove(inners);
	t.equal(outers.length, 2);
	t.equal(inners.length, 0);
	t.equal(
		wrapper.innerHTML,
		'<div class="outer"></div>' +
		'<div class="outer"></div>'
	);
});

test('qq.ajax', function (t) {
	var
		server = sinon.fakeServer.create(),
		preloaderEl = qq.create({
			id: 'preloader', className: 'hide' }),
		i = 1, j;

	function cb(responseText) {
		if (!i) {
			server.restore();
		}
		t.deepEqual(
			JSON.parse(responseText),
			[{ success: true, data: 'hi' }]
		);
		if (!i) {
			t.ok('after all', preloaderEl.classList.contains('hide') &&
				!preloaderEl.classList.contains('show'));
		} else {
			t.ok('before next', preloaderEl.classList.contains('show') &&
				!preloaderEl.classList.contains('hide'));
			i--;
		}
	}

	server.respondWith('GET', '/url',
		[200, { 'Content-Type': 'application/json' },
		'[{ "success": true, "data": "hi" }]']);
	wrapper.appendChild(preloaderEl);

	// 2 asserts for every callback
	t.plan(2 + (i + 1) * 2);
	// preloader is not defined
	t.throws(qq.ajax.bind(null, '/url', function () {}));

	qq.ajax.setPreloader(preloaderEl);
	t.ok('before all', preloaderEl.classList.contains('hide') &&
		!preloaderEl.classList.contains('show'));
	for (j = i; j >= 0; j--) {
		qq.ajax('/url', cb);
	}
	server.respond();
});
