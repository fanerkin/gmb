# userjs for playelephant

### building
* `npm run build`
* `npm run watch` - watching for incremental updates in development

### linting & testing
* `npm run lint`
* `npm run test`, opening link provided runs tests in the browser

### usage
* use generated `dist/user.js` with greasemonkey/tampermonkey

### license
MIT
